#!/usr/bin/env python3

import sys
from threading import Thread
import paho.mqtt.client as mqtt

from sprut import SprutAPI
from config import Config
from graphite import Graphite

config = Config()
sprut = SprutAPI(config.sprut['username'], config.sprut['password'], config.sprut['server'])
graphite = Graphite(config.grafana['graphite']['user'], config.grafana['apikey'], config.grafana['graphite']['metrics_url'])


class SpruthubMQTTClient(mqtt.Client):
    """MQTT Client for the SprutHub"""
    def on_connect(self, mqttc, obj, flags, rc):
        if rc == 0:
            self.subscribe("/spruthub/accessories/#", 0)

    def on_message(self, mqttc, obj, msg):
        # print(f'SPRUT MQTT > {msg.topic} = {msg.payload}')
        accessory = msg.topic.split('/')
        accessory_id = int(accessory[3])
        service_id = int(accessory[4])
        service_name = accessory[5]
        value_name = accessory[6]
        value_value = msg.payload.decode('utf-8')
        if (accessory_id > 3) and (value_value is not None):  # we do not need to parse system accessories
            metric_name = get_metric_name(accessory_id, service_id, service_name, value_name)
            metric_value = get_metric_value(value_value)
            if metric_name:
                # print(f'SPRUT MQTT > {metric_name} = {metric_value}')
                graphite.push_metric(metric_name, metric_value)


def get_metric_name(accessory_id, service_id, service_name, value_name):
    """Generate a Graphite-style metric name from the MQTT message topic
    :returns: metric name in Graphite format or `None` if service is a system or value is a Name
    """
    if (service_id not in [1, 8]) and (value_name != 'Name'):  # services with id = [1, 8] are the system information
        accessory = sprut.get_accessory_by_id(accessory_id)
        if accessory is None:  # it seems like an accessory were added after an api was requested...
            sprut.load_accessories()  # ...so, reloading accessories from the sprut
            accessory = sprut.get_accessory_by_id(accessory_id)

        svc = sprut.get_service_by_id(accessory_id, service_id)
        aid = accessory['id']
        room = accessory['roomName']
        name = accessory['name']
        service = svc['name']
        metric_name = f'{room}.{name}.{aid}.{service}.{value_name}'.lower().replace(' ', '_')
        return metric_name
    else:
        return None


def get_metric_value(value):
    """Set a metric value
    :returns: metric value as a `float`
    """
    try:
        return float(value)
    except:
        if value.lower() == 'true':
            return float(True)
        else:
            return float(False)


def main():
    print(f'Starting a Marriage between Sprut & Graphite')

    sprut.auth()
    sprut.load_accessories()

    sprut_mqtt = SpruthubMQTTClient(client_id="SGM")
    sprut_mqtt.connect(config.sprut['server'], config.sprut['mqtt_port'], config.sprut['mqtt_timeout'])

    try:
        sprut_mqtt_thread = Thread(target=sprut_mqtt.loop_forever)
        # sprut_mqtt_thread.daemon = True
        sprut_mqtt_thread.start()
        sprut_mqtt_thread.join()
    except (KeyboardInterrupt, SystemExit):
        print('Keyboard interrupt or SystemExit, quitting MQTT threads.')
        sprut_mqtt.disconnect()
        sys.exit()


if __name__ == '__main__':
    main()
