import json
import requests


class SprutAPI:
    """
    Main Class for the SprutHub API
    """
    def __init__(self, username, password, server, port=55555):
        self.server = server       # SprutHub address
        self.port = port           # SprutHub API Port
        self.username = username   # SprutHub Username
        self.password = password   # SprutHub Password
        self.token = None          # SprutHub Auth Token
        self.accessories = []      # Accessories list

    def auth(self):
        """Authentication at SprutHub"""
        try:
            response = requests.post(
                url=f'http://{self.server}:{self.port}/api/server/login/{self.username}',
                headers={'Content-Type': 'application/json'},
                data=self.password
            )
            if response.status_code == 200:
                print('SPRUT API > Authorized at SprutHub')
                token = response.content.decode()
                self.token = json.loads(token)['token']
                print(f'SPRUT API > Token: {self.token}')
                return self.token
            else:
                print(f'SPRUT API > Authorization failed with status code {response.status_code}')
                return None
        except requests.exceptions.RequestException as e:
            print(f'SPRUT API > HTTP Request Error: {e}')

    def load_accessories(self):
        """Load all the accessories from the SprutHub"""
        try:
            response = requests.get(
                url=f'http://{self.server}:{self.port}/api/accessories',
                headers={'Content-Type': 'application/json', 'Cookie': f'token={self.token}'},
                params={'expand': 'services,characteristics'},
            )
            if response.status_code == 200:
                accessories = response.content.decode()
                self.accessories = json.loads(accessories)
                print(f'SPRUT API > Loaded {len(self.accessories)} accessories')
                # print(f'SPRUT API > Accessories ({len(self.accessories)}): {self.accessories}')
                return self.accessories
            else:
                print(f'SPRUT API > Load accessories load failed with status code {response.status_code}')
                return None
        except requests.exceptions.RequestException as e:
            print(f'SPRUT API > HTTP Request Error: {e}')
        except json.JSONDecodeError as e:
            print(f'SPRUT API > JSON Decode Error: {e}')

    def get_accessory_by_id(self, aid):
        """Get an accessory by sprut identifier. Accessories must be loaded from sprut.
        :param aid: SprutHub accessory identifier
        :returns: accessory object or `None` if not found
        """
        return next((accessory for accessory in self.accessories if accessory['id'] == aid), None)

    def get_service_by_id(self, aid, sid):
        """Get a service by sprut identifiers. Accessories must be loaded from sprut.
        :param aid: SprutHub accessory identifier
        :param sid: SprutHub service identifier for the accessory
        :returns: service object or `None` if not found
        """
        accessory = self.get_accessory_by_id(aid)
        return next((service for service in accessory['services'] if service['id'] == sid), None)
