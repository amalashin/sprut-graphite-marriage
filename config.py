import yaml

config_file = 'config.yaml'


class Config:
    """
    Configuration for the app
    """
    def __init__(self):
        self.sprut = None       # spruthub settings
        self.grafana = None     # grafana cloud settings
        self.load()

    def load(self):
        """Load configuration from the file"""
        try:
            with open(config_file, 'r', encoding='utf8') as f:
                obj = yaml.load(f, Loader=yaml.FullLoader)
                self.sprut = obj['sprut']
                self.grafana = obj['grafana']
        except IOError as e:
            print(f'CONFIG > Config file error: {e}')
        except yaml.YAMLError as e:
            print(f'CONFIG > Error {e}')

    def write(self):
        """Write configuration to the file"""
        with open(config_file, 'w', encoding='utf8') as f:
            try:
                obj = {'sprut': self.sprut, 'grafana': self.grafana}
                data = yaml.dump(obj, f, allow_unicode=True)
            except yaml.YAMLError as e:
                print(f'CONFIG > Error {e}')
