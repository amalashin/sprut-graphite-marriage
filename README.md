# Установка
0. Залогиниться под пользователем *pi*
1. Скопировать файлы в `/home/pi/sprut-graphite-marriage`
2. Выполнить команды (установка зависимостей и создание сервиса)
   ```bash
   cd ~/sprut-graphite-marriage
   sudo apt-get install python3-pip python3-venv
   sudo pip3 install virtualenv
   python3 -m venv venv
   source ./venv/bin/activate
   pip install -r requirements.txt
   sudo mv sprut-graphite-marriage.service /etc/systemd/system/
   ```

# Настройка
0. Отредактировать `config.yaml` в соответствии с системой

# Управление
0. Проверка сервиса: `./venv/bin/python -u main.py`
1. Запуск сервиса: `sudo systemctl start sprut-graphite-marriage`
2. Проверка сервиса: `sudo systemctl status sprut-graphite-marriage`
3. Остановка сервиса: `sudo systemctl stop sprut-graphite-marriage`
4. Включить в автозапуск `sudo systemctl enable sprut-graphite-marriage`
5. Выключить в автозапуск `sudo systemctl disable sprut-graphite-marriage`